<?php

class Solution1
{
    private int $min;
    private int $max;

    public function __construct($min = 1, $max = 100)
    {
        $this->min = $min;
        $this->max = $max;
    }

    public function printLadder()
    {
        $number = $this->min;
        $ladder_step = 1;
        while($number <= $this->max) {
            for ($i = 0; ($i < $ladder_step && $number <= $this->max); $i++) {
                echo $number . ' ';
                $number++;
            }
            echo "\r\n";
            $ladder_step++;
        }
    }
}

$result = (new Solution1())->printLadder();