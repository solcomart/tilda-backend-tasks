Задача с отображением номера телефона.

Во-первых, сразу поменять заглушку "8-800-DIGITS" на актуальный номер телефона компании, на который можно дозвониться из любого города и попасть на "общий" колл-центр.

Далее вижу 3 варианта выводить "правильный" телефон пользователю:
1. Определять город пользователя через Geolocation API, но это пожалуй подходит только для случаев, когда у нас уже есть разрешение на использование геолокации от пользователя. Запрашивать доступ к геолокации только для отображения правильного номера телефона нецелесообразно.
2. Приблизительно определять геопозицию пользователя на основании IP адреса, используя подобный сервис: https://geolocation-db.com/json/ Если компания присутствует в найденном городе, подменять "общий" номер телефона на нужный.
3. Не ориентироваться на геопозицию пользователя, показывать номер телефона головного филиала + город, в котором находится головной филиал. Предложить пользователю указать другой город из списка, запомнить его выбор (сохранить в cookies) и на основании выбранного города показывать нужный номер телефона на всех страницах.

**В зависимости от размера компании и посещаемости сайта, я остановлюсь на втором варианте (определение локации через IP с помощью сервиса), либо задумаюсь о разработке отдельного микросервиса для определения города пользователя по IP адресу.**

 

