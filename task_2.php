<?php

class Solution2
{
    private int $width;
    private int $height;
    private int $min;
    private int $max;

    public function __construct($width = 5, $height = 7, $min = 1, $max = 1000)
    {
        $this->min = $min;
        $this->max = $max;
        $this->width = $width;
        $this->height = $height;
    }

    public function fillArrayWithRandomUniqueNumbers() {
        $numbers = $this->getUniqueNumbers($this->width * $this->height, $this->min, $this->max);
        $row_sums = [];
        $col_sums = [];
        $array = [];
        $i = 0;
        for ($h = 0; $h < $this->height; $h++) {
            $current_row_sum = 0;
            for ($w = 0; $w < $this->width; $w++) {
                $array[$h][$w] = $numbers[$i];
                $current_row_sum += $numbers[$i];
                echo $numbers[$i] . "\t";
                $i++;
            }
            $row_sums[] = $current_row_sum;
            echo "\r\n";
        }

        for ($w = 0; $w < $this->width; $w++) {
            $current_col_sum = 0;
            for ($h = 0; $h < $this->height; $h++) {
                $current_col_sum += $array[$h][$w];
            }
            $col_sums[] = $current_col_sum;
        }

        echo "\r\nСуммы строк: ";
        echo implode(', ', $row_sums);
        echo "\r\nСуммы столбцов: ";
        echo implode(', ', $col_sums);
    }

    public function getUniqueNumbers($quantity, $min, $max)
    {
        $numbers = [];
        while($quantity > 0) {
            $random = mt_rand($min, $max);
            if (!array_key_exists($random, $numbers)) {
                $numbers[$random] = null;
                $quantity--;
            }
        }
        return array_keys($numbers);
    }
}

$result = (new Solution2())->fillArrayWithRandomUniqueNumbers();